//
//  Commit.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface Commit : NSObject

@property(nonatomic, strong) NSString *hashCommit;
@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSString *date;
@property(nonatomic, strong) User *user;


@end
