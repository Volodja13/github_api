//
//  Model.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "Model.h"

@implementation Model

- (instancetype)init{
    self = [super init];
    if(self)
    {
        self->_repos = [@[] mutableCopy];
        self->_commits = [@[] mutableCopy];
    }
    return self;
}

@end
