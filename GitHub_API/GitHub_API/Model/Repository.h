//
//  Repository.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface Repository : NSObject

@property(nonatomic, strong) User *user;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *descriptionRep;
@property(nonatomic, strong) NSNumber *countForks;
@property(nonatomic, strong) NSNumber *countWatchers;

@end
