//
//  Model.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@class User;
@class Repository;
@class Commit;

@interface Model : NSObject

@property(nonatomic, strong) User *user;
@property(nonatomic, strong) NSArray<Repository*> *repos;
@property(nonatomic, strong) NSArray<Commit*> *commits;

@end
