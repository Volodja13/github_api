//
//  CommitsTableViewController.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CommitsTableViewController.h"
#import "Model.h"
#import "Repository.h"
#import "ServicesLocator.h"
#import "CommitsService.h"
#import "ErrorParser.h"
#import "CommitTableViewCell.h"
#import "Commit.h"
#import "User.h"

@interface CommitsTableViewController ()

@property(nonatomic, weak) Model* model;
@property(nonatomic, weak) NSIndexPath *indexRepository;

@end

@implementation CommitsTableViewController


NSString * const CommitReusableID = @"CommitID";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.allowsSelection = NO;
    
    [self startConnection];
    [[[ServicesLocator shared] commitsService] commitsUser:self.model.user ForRepository:self.model.repos[self.indexRepository.row] completion:^(NSError *error, NSArray *commits) {
        if(error){
            [self finishConnection];
        }
        else if(commits){
            [self finishConnection];
            self.model.commits = commits;
            [self.tableView reloadData];
        }
    }];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    self.model.commits = [@[] mutableCopy];
}

- (void)setWeakReferenceOnModel:(Model*)model{
    self->_model = model;
}


- (void)setIndexPathForRepository:(NSIndexPath*)indexpath{
    self->_indexRepository = indexpath;
}

- (void) startConnection{
    [self.navigationItem setTitle:@"Loading"];
    
}

- (void) finishConnection{
    Repository *repository = self.model.repos[self.indexRepository.row];
    [self.navigationItem setTitle:repository.name];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model.commits count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CommitReusableID forIndexPath:indexPath];
    
    Commit *commit = self.model.commits[indexPath.row];
    
    [cell setCommit:commit];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:commit.user.avatarUrl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [cell setUserAvatar: [self imageWithImage:image scaledToSize:[cell getSizeImageView]]];
    }];
    
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 246;
}


@end
