//
//  ReposTableViewController.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ReposTableViewController.h"
#import "Model.h"
#import "User.h"
#import "ServicesLocator.h"
#import "ReposService.h"
#import "ErrorParser.h"
#import "RepositoryTableViewCell.h"
#import "CommitsTableViewController.h"

@interface ReposTableViewController ()

@property(nonatomic, weak) Model* model;

@end

@implementation ReposTableViewController

NSString * const RepositoryReusableID = @"RepositoryID";
NSString * const SegueCommitsID = @"SegueCommitsID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:self.model.user.name];
    UIBarButtonItem *logOutButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"LogOut"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(logOutAction)];
    self.navigationItem.leftBarButtonItem = logOutButton;
    
    [self startConnection];
    [[[ServicesLocator shared] reposService] reposForUser:nil completion:^(NSError *error, NSArray *repos) {
        if(error){
            [self finishConnection];
            [ErrorParser showAllertWithError:error];
        }
        else if(repos){
            [self finishConnection];
            self.model.repos = repos;
            [self.tableView reloadData];
        }
    }];
}



- (void) startConnection{
    [self.navigationItem setTitle:@"Loading"];
    
}

- (void) finishConnection{
    [self.navigationItem setTitle:@"Repos"];
}

-(IBAction)logOutAction{
    self.model.repos = [@[] mutableCopy];
    [self.parentViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setWeakReferenceOnModel:(Model*)model{
    self->_model = model;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model.repos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RepositoryReusableID forIndexPath:indexPath];
    
    Repository *repository = self.model.repos[indexPath.row];
    
    [cell setRepository:repository];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:repository.user.avatarUrl options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [cell setUserAvatar: [self imageWithImage:image scaledToSize:[cell getSizeImageView]]];
    }];
    
    
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:SegueCommitsID])
    {
        [(CommitsTableViewController*)[segue destinationViewController] setWeakReferenceOnModel:self.model];
        [(CommitsTableViewController*)[segue destinationViewController] setIndexPathForRepository:[self.tableView indexPathForSelectedRow]];
    }
}


@end
