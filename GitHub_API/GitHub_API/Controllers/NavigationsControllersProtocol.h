//
//  NavigationsControllersProtocol.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Model;

@protocol NavigationsControllersProtocol <NSObject>

- (void)setWeakReferenceOnModel:(Model*)model;

@end
