//
//  AutorizationViewController.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "AutorizationViewController.h"
#import "TransportLayer.h"
#import "ServicesLocator.h"
#import "UserService.h"
#import "Assembly.h"
#import "ErrorParser.h"
#import "User.h"
#import "Model.h"
#import "NavigationViewController.h"
#import <Security/Security.h>
#import <KeychainItemWrapper/KeychainItemWrapper.h>

@interface AutorizationViewController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation AutorizationViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Assembly assemblyServiceLocator];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    self.loginTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"GitHub_api" accessGroup:nil];
    
    self.loginTextField.text = [keychainItem objectForKey:(__bridge id)kSecAttrAccount];
    self.passwordTextField.text = [keychainItem objectForKey:(__bridge id)kSecValueData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void) dismissKeyboard{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)sendAction:(id)sender {
    [self startConnection];
    [[TransportLayer manager] setParametrForBasicAutorizationWithLogin:self.loginTextField.text password:self.passwordTextField.text ];
    
    [[[ServicesLocator shared] userService] userWithCompletion:^(NSError *error, User *user) {
        if(error){
            [self finishConnection];
            [ErrorParser showAllertWithError:error];
        }
        else if(user){
            [self finishConnection];
            
            KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"GitHub_api" accessGroup:nil];
            
            [keychainItem setObject:self.loginTextField.text forKey:(__bridge id)kSecAttrAccount];
            [keychainItem setObject:self.passwordTextField.text forKey:(__bridge id)kSecValueData];
            
            
            Model *model = [Model new];
            model.user = user;
            NavigationViewController *navViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NVC"];
            
            [navViewController setModel:model];
            
            [self presentViewController:navViewController animated:YES completion:^{}];
        }
    }];
    
}

- (void) startConnection{
    [self.sendButton setEnabled:NO];
    [self.activityIndicator startAnimating];
}

- (void) finishConnection{
    [self.sendButton setEnabled:YES];
    [self.activityIndicator stopAnimating];
}

@end
