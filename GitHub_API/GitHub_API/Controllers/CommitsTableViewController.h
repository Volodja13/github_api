//
//  CommitsTableViewController.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationsControllersProtocol.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CommitsTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, NavigationsControllersProtocol, SDWebImageManagerDelegate>

- (void)setWeakReferenceOnModel:(Model*)model;
- (void)setIndexPathForRepository:(NSIndexPath*)indexpath;


@end
