//
//  NavigationViewController.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "NavigationViewController.h"
#import "Model.h"
#import "NavigationsControllersProtocol.h"

@interface NavigationViewController ()

@property(nonatomic, strong) Model *model;

@end

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    for (id <NavigationsControllersProtocol> controller in
         [self viewControllers]) {
        [controller setWeakReferenceOnModel:self.model];
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setModel:(Model*)model{
    self->_model = model;
}

@end
