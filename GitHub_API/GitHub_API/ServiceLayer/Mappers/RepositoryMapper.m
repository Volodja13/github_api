//
//  RepositoryMapper.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "RepositoryMapper.h"
#import "Repository.h"
#import "User.h"
#import "UserMapper.h"

@implementation RepositoryMapper

+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    NSMutableArray *result = [@[] mutableCopy];
    
    for (NSDictionary *dJSON in json) {
        Repository *repository = [Repository new];
        repository.name = dJSON[@"name"];
        User *user = [User new];
        user = [UserMapper modelFromJson:dJSON[@"owner"]];
        repository.user = user;
        repository.descriptionRep = ([dJSON[@"description"] isEqual:[NSNull null]]) ? @"Not description" :dJSON[@"description"];
        repository.countWatchers =  dJSON[@"watchers_count"];
        repository.countForks = dJSON[@"forks_count"];
        [result addObject:repository];
    }
    
    return result;
}

@end
