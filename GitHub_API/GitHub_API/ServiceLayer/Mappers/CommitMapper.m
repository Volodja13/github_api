//
//  CommitMapper.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CommitMapper.h"
#import "User.h"
#import "Commit.h"
#import "UserMapper.h"

@implementation CommitMapper

+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    NSMutableArray *result = [@[] mutableCopy];
    
    for (NSDictionary *dJSON in json) {
        Commit *commit = [Commit new];
        commit.hashCommit = dJSON[@"sha"];
        NSDictionary *commitJSON = dJSON[@"commit"];
        NSDictionary *commiterJSONForDate = commitJSON[@"committer"];
        commit.date = commiterJSONForDate[@"date"];
        commit.message = commitJSON[@"message"];
        NSDictionary *commiterJSON = dJSON[@"committer"];
        
        User *user = [User new];
        user = [UserMapper modelFromJson:commiterJSON];
        commit.user = user;
        [result addObject:commit];
    }
    
    return result;
}

@end
