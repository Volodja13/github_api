//
//  UserMapper.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "UserMapper.h"
#import "User.h"


@implementation UserMapper

+ (User*)modelFromJson:(NSDictionary*)json{
    User *user = [User new];
    
    user.name = json[@"login"];
    user.userId = json[@"id"];
    user.avatarUrl = [NSURL URLWithString:json[@"avatar_url"]];
    
    return user;
}

@end
