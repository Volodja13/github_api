//
//  TransportLayer.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void (^SuccessCompletionBlock)(id response);
typedef void (^FailureCompletionBlock)(NSError *error);


@interface TransportLayer : AFHTTPSessionManager

+ (instancetype)manager;

- (void) setParametrForBasicAutorizationWithLogin:(NSString*)login password:(NSString*)password;

- (NSURLSessionDataTask *)makeGETRequestWithRelativePath:(NSString *)relativePath
                                              parameters:(NSDictionary *)parameters
                                                 success:(SuccessCompletionBlock)successBlock
                                                 failure:(FailureCompletionBlock)failure;

@end
