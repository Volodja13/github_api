//
//  ServiceFactory.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ServiceFactory.h"
#import "TransportLayer.h"
#import "UserService.h"
#import "ReposService.h"
#import "CommitsService.h"

@implementation ServiceFactory

- (id<UserServiceProtocol>)createUserService{
    return [[UserService alloc] initWithTransport:[TransportLayer manager]];
}

- (id<ReposServiceProtocol>)createReposService{
    return [[ReposService alloc] initWithTransport:[TransportLayer manager]];
}

- (id<CommitsServiceProtocol>)createCommitsService{
    return [[CommitsService alloc] initWithTransport:[TransportLayer manager]];
}



@end
