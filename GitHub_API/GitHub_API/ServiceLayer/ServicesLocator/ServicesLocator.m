//
//  ServicesLocator.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ServicesLocator.h"
#import "ServiceFactoryProtocol.h"

@protocol UserServiceProtocol;
@protocol ReposServiceProtocol;
@protocol CommitsServiceProtocol;

@interface ServicesLocator()

@property (nonatomic, strong) id <ServiceFactoryProtocol> serviceFactory;

@end

@implementation ServicesLocator

static ServicesLocator *sharedInstance = nil;

+ (instancetype)shared {
    return [self sharedService];
}

+ (instancetype)sharedService {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)setServiceFactory:(id <ServiceFactoryProtocol>)serviceFactory {
    _serviceFactory = serviceFactory;
}

- (id <UserServiceProtocol>) userService{
    return [self.serviceFactory createUserService];
}

- (id <ReposServiceProtocol>) reposService{
    return [self.serviceFactory createReposService];
}

- (id <CommitsServiceProtocol>) commitsService{
    return [self.serviceFactory createCommitsService];
}

@end
