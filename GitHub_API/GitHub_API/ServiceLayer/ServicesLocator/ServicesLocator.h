//
//  ServicesLocator.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServiceFactoryProtocol;
@protocol UserServiceProtocol;
@protocol ReposServiceProtocol;
@protocol CommitsServiceProtocol;

@interface ServicesLocator : NSObject

@property (nonatomic, strong, readonly) id <UserServiceProtocol> userService;
@property (nonatomic, strong, readonly) id <ReposServiceProtocol> reposService;
@property (nonatomic, strong, readonly) id <CommitsServiceProtocol> commitsService;

+ (instancetype)shared;
- (void)setServiceFactory:(id <ServiceFactoryProtocol>)serviceFactory;

@end
