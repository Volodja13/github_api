//
//  ServiceFactoryProtocol.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserServiceProtocol;
@protocol ReposServiceProtocol;
@protocol CommitsServiceProtocol;

@protocol ServiceFactoryProtocol <NSObject>

- (id<UserServiceProtocol>)createUserService;
- (id<ReposServiceProtocol>)createReposService;
- (id<CommitsServiceProtocol>)createCommitsService;

@end
