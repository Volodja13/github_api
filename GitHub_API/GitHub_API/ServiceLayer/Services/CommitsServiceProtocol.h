//
//  CommitsServiceProtocol.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransportLayer;
@class User;
@class Repository;

typedef void (^CommitsListCompletionBlock)(NSError *error, NSArray *commits);

@protocol CommitsServiceProtocol <NSObject>

- (instancetype)initWithTransport:(TransportLayer*)transport;

- (NSURLSessionDataTask *)commitsUser:(User*)user
                        ForRepository:(Repository*)repository
                           completion:(CommitsListCompletionBlock)completion;

@end
