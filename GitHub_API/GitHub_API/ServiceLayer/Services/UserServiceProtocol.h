//
//  UserServiceProtocol.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransportLayer;
@class User;

typedef void (^UserCompletionBlock)(NSError *error, User *user);

@protocol UserServiceProtocol <NSObject>

- (instancetype)initWithTransport:(TransportLayer*)transport;

- (NSURLSessionDataTask *)userWithCompletion:(UserCompletionBlock)completion;

@end
