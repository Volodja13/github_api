//
//  UserService.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "UserService.h"
#import "TransportLayer.h"
#import "User.h"
#import "UserMapper.h"

@interface UserService ()

@property(nonatomic, strong) TransportLayer *transport;

@end

@implementation UserService

- (instancetype)initWithTransport:(TransportLayer*)transport{
    self = [super init];
    if(self)
    {
        _transport = transport;
    }
    return self;
}

- (NSURLSessionDataTask *)userWithCompletion:(UserCompletionBlock)completion{
    
    NSString *relativePath = @"/user";
    NSMutableDictionary *parametrs = [@{} mutableCopy];
    return [self.transport makeGETRequestWithRelativePath:relativePath
                                               parameters:parametrs
                                                  success:^(id response) {
                                                      [self handleSuccsesJson:response completion:completion];
                                                  }
                                                  failure:^(NSError *error) {
                                                      completion(error, nil);
                                                  }];
    
    
    
}

- (void)handleSuccsesJson:(NSDictionary*)response completion:(UserCompletionBlock)completion{
    User *user = [UserMapper modelFromJson:response];
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(nil, user);
    });
}


@end
