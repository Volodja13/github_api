//
//  ReposService.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ReposService.h"
#import "TransportLayer.h"
#import "RepositoryMapper.h"

@interface ReposService ()

@property(nonatomic, strong) TransportLayer *transport;

@end

@implementation ReposService

- (instancetype)initWithTransport:(TransportLayer*)transport{
    self = [super init];
    if(self)
    {
        _transport = transport;
    }
    return self;
}

- (NSURLSessionDataTask *)reposForUser:(User*)user
                            completion:(ReposListCompletionBlock)completion{
    
    NSString *relativePath = @"/user/repos";
    NSMutableDictionary *parametrs = [@{} mutableCopy];
    return [self.transport makeGETRequestWithRelativePath:relativePath
                                               parameters:parametrs
                                                  success:^(id response) {
                                                      [self handleSuccsesJson:response completion:completion];
                                                  }
                                                  failure:^(NSError *error) {
                                                      completion(error, nil);
                                                  }];
    
    
    
}

- (void)handleSuccsesJson:(NSDictionary*)response completion:(ReposListCompletionBlock)completion{
    NSMutableArray *res = [RepositoryMapper modelFromJson:response];
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(nil, res);
    });
}

@end
