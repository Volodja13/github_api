//
//  CommitsService.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CommitsService.h"
#import "TransportLayer.h"
#import "User.h"
#import "Repository.h"
#import "CommitMapper.h"

@interface CommitsService ()

@property(nonatomic, strong) TransportLayer *transport;

@end

@implementation CommitsService

- (instancetype)initWithTransport:(TransportLayer*)transport{
    self = [super init];
    if(self)
    {
        _transport = transport;
    }
    return self;
}

- (NSURLSessionDataTask *)commitsUser:(User*)user
                        ForRepository:(Repository*)repository
                           completion:(CommitsListCompletionBlock)completion{
    
    NSString *relativePath = [NSString stringWithFormat:@"/repos/%@/%@/commits", user.name, repository.name];
    NSMutableDictionary *parametrs = [@{} mutableCopy];
    return [self.transport makeGETRequestWithRelativePath:relativePath
                                               parameters:parametrs
                                                  success:^(id response) {
                                                      [self handleSuccsesJson:response completion:completion];
                                                  }
                                                  failure:^(NSError *error) {
                                                      completion(error, nil);
                                                  }];
    
    
    
}

- (void)handleSuccsesJson:(NSDictionary*)response completion:(CommitsListCompletionBlock)completion{
    NSMutableArray *res = [CommitMapper modelFromJson:response];
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(nil, res);
    });
}

@end
