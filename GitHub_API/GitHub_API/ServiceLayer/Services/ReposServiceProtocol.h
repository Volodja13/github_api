//
//  ReposServiceProtocol.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransportLayer;
@class User;

typedef void (^ReposListCompletionBlock)(NSError *error, NSArray *repos);

@protocol ReposServiceProtocol <NSObject>

- (instancetype)initWithTransport:(TransportLayer*)transport;

- (NSURLSessionDataTask *)reposForUser:(User*)user
                            completion:(ReposListCompletionBlock)completion;

@end
