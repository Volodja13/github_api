//
//  Assembly.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "Assembly.h"
#import "ServicesLocator.h"
#import "ServiceFactory.h"

@implementation Assembly

+ (void)assemblyServiceLocator{
    ServicesLocator *servicesLocator = [ServicesLocator shared];
    [servicesLocator setServiceFactory:[ServiceFactory new]];
}

@end
