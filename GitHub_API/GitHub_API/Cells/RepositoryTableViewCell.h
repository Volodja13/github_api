//
//  RepositoryTableViewCell.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repository.h"

@interface RepositoryTableViewCell : UITableViewCell

- (void) setRepository:(Repository*)repository;
- (void) setUserAvatar:(UIImage*)image;
- (CGSize) getSizeImageView;

@end
