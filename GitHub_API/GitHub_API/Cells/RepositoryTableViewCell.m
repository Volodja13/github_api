//
//  RepositoryTableViewCell.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import "Repository.h"
#import "User.h"

@interface RepositoryTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageAvatarUser;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelRepositoryName;
@property (weak, nonatomic) IBOutlet UILabel *labelRepositoryDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelRepositoryStatistic;


@end

@implementation RepositoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}


- (void) setRepository:(Repository*)repository{
    self.labelUserName.text = repository.user.name;
    self.labelRepositoryName.text = repository.name;
    self.labelRepositoryDescription.text = repository.descriptionRep;
    self.labelRepositoryStatistic.text = [NSString stringWithFormat:@"Forks: %@ Watchers: %@", repository.countForks, repository.countWatchers];
}


- (void) setUserAvatar:(UIImage*)image{
    self.imageAvatarUser.image = image;
}

- (CGSize) getSizeImageView{
    return self.imageAvatarUser.frame.size;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
