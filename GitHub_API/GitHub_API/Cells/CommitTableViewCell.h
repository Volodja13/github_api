//
//  CommitTableViewCell.h
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Commit;

@interface CommitTableViewCell : UITableViewCell

- (void) setCommit:(Commit*)commit;
- (void) setUserAvatar:(UIImage*)image;
- (CGSize) getSizeImageView;

@end
