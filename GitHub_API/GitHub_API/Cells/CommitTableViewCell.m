//
//  CommitTableViewCell.m
//  GitHub_API
//
//  Created by Vovan on 19.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "CommitTableViewCell.h"
#import "Commit.h"
#import "User.h"

@interface CommitTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewAvetarUser;
@property (weak, nonatomic) IBOutlet UILabel *labelUser;

@property (weak, nonatomic) IBOutlet UILabel *labelHash;
@property (weak, nonatomic) IBOutlet UILabel *labelComment;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@end

@implementation CommitTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (void) setCommit:(Commit*)commit{
    self.labelUser.text = commit.user.name;
    self.labelHash.text = commit.hashCommit;
    self.labelComment.text = commit.message;
    self.labelDate.text = commit.date;
}

- (void) setUserAvatar:(UIImage*)image{
    self.imageViewAvetarUser.image = image;
}

- (CGSize) getSizeImageView{
    return self.imageViewAvetarUser.frame.size;
}

@end
